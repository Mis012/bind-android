# bind-android

Guide + scripts for compiling and installing berkeley internet name domain (bind) on android phone. Tested with LineageOS 13 (marshmallow).

root access required!

# dependencies

\*xz utils
https://sourceforge.net/projects/lzmautils/

\*libxml2
ftp://xmlsoft.org/libxml2/

\*openssl
https://github.com/openssl/openssl

# compiling
\*make a standalone toolchain in ~/Android/ndk-standalone  
`.../ndk-bundle/build/tools/make-standalone-toolchain.sh --platform=android-23 --install-dir=~/Android/ndk-standalone --arch=arm`        
\*edit [envsetup.sh](https://gitlab.com/Mis012/bind-android/blob/master/envsetup.sh) to match the paths where you extracted the dependencies      
\*`source envsetup.sh`    
\*compile liblzma:  
`./configure --host=arm-linux-androideabi --target=arm-linux-androideabi --enable-static --with-pic --disable-xz --disable-xzdec --disable-lzmainfo --disable-scripts --disable-lzmadec --enable-shared --prefix=$PWD`  
`make`  
`make install`  
\*compile libxml2:  
`./configure --host=arm-linux-androideabi --without-python --prefix=$PWD`  
`make`  
edit cp to rsync in Makefile to make it happily install includes over themselves (or install out-of-tree)  
`make install`  
\*compile bind:  
`./configure --host=arm-linux-androideabi --with-ecdsa=yes --with-eddsa=all --disable-linux-caps --prefix=/system/ --sysconfdir=/system/etc --localstatedir=/data/var --with-openssl=$OPENSSL_HOME --with-pic`  
`make`  
if you want to use target prefix (just in case it sometimes cares about it):  
`sudo mkdir /system && sudo mkdir /data && sudo chown *you* /system && sudo chown *you* /data`  
`make install`  

# deploying

make a directory somewhere and name it *system*.  
Inside this directory, place:  
`/system/etc`, `/system/bin` and `/system/sbin` that you obtained in previous step,  
a directory called *lib*;  
inside *lib* aggregate the contents of: `${LZMA_HOME}/lib`, `${LIBXML2_HOME}/lib`,`${OPENSSL_HOME}/lib`  
delete everything there that's not a shared object, including the *libcrypto.so* and *libssl.so* symlinks 
(unless you want to find out if it's binary compatible with the ones you already have. (hint: unlikely|bootloop))

now you have:  
```
system
  |
  + - bin/
  |    
  + - etc/
  |
  + - lib/
  \
   `- sbin/
```
now, connect your phone with adb debugging enabled, and run:  
`adb root`  
`adb shell "mount -o remount,rw /system"`  
`adb push ./system /system`  

to test if the binaries work, try `named -g`, which should output log ending with an error.

# finalization

now, you have named running. but not for long. despite the fact that we set the /var path to /data/var, it still wants to use /var. but there's none.

putting the following inside an init.d script fixes that:
```
#named wants /var at.. /var
mount -o remount,rw /
mkdir /var
mount -o remount,ro /
mount --bind /data/var /var
```

now, you can just supply an /etc/named.conf (and possibly other needed files) and named should work.

to launch it on startup (+log :)  
add `named -L /var/named.log` to the init.d script.

to test, try `nslookup gitlab.com`  
you should get output confirming that 127.0.0.1:53 has been used to retrieve the information.
the query should generate log output as well. what will NOT generate a log output, is any other application using DNS.
this is because bind cannot dictate the global dns settings. but *you* can. let's do it!  
https://forum.xda-developers.com/general/xda-university/guide-how-to-change-dns-android-device-t3273769  
has some in-depth info on that. nice ;)  
I like this one:  
```
iptables -t nat -A OUTPUT -p tcp --dport 53 -j DNAT --to-destination 127.0.0.1:53
iptables -t nat -A OUTPUT -p udp --dport 53 -j DNAT --to-destination 127.0.0.1:53
iptables -t nat -D OUTPUT -p tcp --dport 53 -j DNAT --to-destination 127.0.0.1:53 || true
iptables -t nat -D OUTPUT -p udp --dport 53 -j DNAT --to-destination 127.0.0.1:53 || true
iptables -t nat -I OUTPUT -p tcp --dport 53 -j DNAT --to-destination 127.0.0.1:53
iptables -t nat -I OUTPUT -p udp --dport 53 -j DNAT --to-destination 127.0.0.1:53
```
because it should work even on apps that want to use their own dns service.

however, it was clearly not intended for use with bind on localhost - because it will redirect it's requests as well.
now you are probably thinking that we will have to add an exemption for root nameservers. but there is another way.

```
iptables -t nat -A OUTPUT -m owner ! --gid-owner 1 -p tcp --dport 53 -j DNAT --to-destination 127.0.0.1:53
iptables -t nat -A OUTPUT -m owner ! --gid-owner 1 -p udp --dport 53 -j DNAT --to-destination 127.0.0.1:53
iptables -t nat -D OUTPUT -m owner ! --gid-owner 1 -p tcp --dport 53 -j DNAT --to-destination 127.0.0.1:53 || true
iptables -t nat -D OUTPUT -m owner ! --gid-owner 1 -p udp --dport 53 -j DNAT --to-destination 127.0.0.1:53 || true
iptables -t nat -I OUTPUT -m owner ! --gid-owner 1 -p tcp --dport 53 -j DNAT --to-destination 127.0.0.1:53
iptables -t nat -I OUTPUT -m owner ! --gid-owner 1 -p udp --dport 53 -j DNAT --to-destination 127.0.0.1:53
```
I can hear you asking: how do I get named to have GID of 1? well, it's really easy:
```
chown root:1 /system/sbin/named
chmod g+s /system/sbin/named
```
that should be it!
now for IPv6.. if you have nat v6, you can use the same rules. otherwise, disable it?

the complete init script is in the repo: [98named](https://gitlab.com/Mis012/bind-android/blob/master/98named)