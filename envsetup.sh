export HOME_NAME=Mis012

export MY_NDK=/home/${HOME_NAME}/Android/ndk-standalone/
export SYSROOT=${MY_NDK}/sysroot
export PATH=${MY_NDK}/bin:$PATH

export CPP=arm-linux-androideabi-cpp
export AR=arm-linux-androideabi-ar
export AS=arm-linux-androideabi-as
export NM=arm-linux-androideabi-nm
export CC=arm-linux-androideabi-gcc
export CXX=arm-linux-androideabi-g++
export LD=arm-linux-androideabi-ld
export RANLIB=arm-linux-androideabi-ranlib


export OPENSSL_HOME=/home/${HOME_NAME}/Github_and_other_sources/openssl/
export LZMA_HOME=/home/${HOME_NAME}/Github_and_other_sources/xz-5.2.4/
export LIBXML2_HOME=/home/${HOME_NAME}/Github_and_other_sources/libxml2-2.9.8/
export CPPFLAGS="${CPPFLAGS} -I${OPENSSL_HOME}/include -I${LZMA_HOME}/include/ -I${LIBXML2_HOME}/include"
export CXXFLAGS="${CXXFLAGS} -fPIC -Os"
export LDFLAGS="-fPIC -pie ${LDFLAGS} -L${OPENSSL_HOME}/lib -L${LZMA_HOME}/lib -L${LIBXML2_HOME}/lib"

export BUILD_CC=gcc
